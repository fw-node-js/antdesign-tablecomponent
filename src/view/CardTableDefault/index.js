const App = ({ dataSource, columns }) => {
  const colDefault = [{ index: "label" }, { index: "name" }];
  const [cols, setCols] = useState([]);
  const [rows, setRows] = useState([]);
  useEffect(() => {
    setRows(
      dataSource?.map((item) => ({
        ...item,
        level: 0,
      }))
    );
  }, [dataSource]);
  useEffect(() => {
    setCols(
      colDefault.map((item) => {
        item = {
          ...item,
          onCell: (record, rowIndex) => {
            if (item.expand) {
              return {
                onClick: (event) => {
                  console.log(rows);
                },
              };
            }
          },
        };
        return item;
      })
    );
  }, [columns]);
};
