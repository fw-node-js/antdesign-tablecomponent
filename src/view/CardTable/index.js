import { useQuery } from "react-query";
import apiController from "../../api";
import TableExpand from "../../components/TableExpand";
import columns from "./columns";
import columns2 from "./columns2";
const CardTable = () => {
  const { data: bookingRows } = useQuery(["get-bookingRows"], () =>
    apiController().getData(500)
  );
  const { data: bookingRows2 } = useQuery(["get-bookingRows2"], () =>
    apiController().getData2(1000)
  );
  return (
    <>
      <TableExpand
        rowKey="BOOKING_ID"
        dataSource={bookingRows?.ITR_BOOKING_HDR}
        columns={columns}
        scroll={{ x: 1800, y: 800 }}
        onRow={(record, rowIndex) => {
          return {
            onDoubleClick: (event) => {
              console.log("onDoubleClick");
            }, // double click row
            // onMouseEnter: (event) => {console.log("onMouseEnter")}, // mouse enter row
            // onMouseLeave: (event) => {console.log("onMouseLeave")}, // mouse leave row
          };
        }}
      />

      {/* <TableExpand
        rowKey="BOOKING_ID"
        dataSource={bookingRows2?.ITR_BOOKING_HDR}
        columns={columns2}
        scroll={{ x: 1800, y: 800 }}
      /> */}
    </>
  );
};
export default CardTable;
