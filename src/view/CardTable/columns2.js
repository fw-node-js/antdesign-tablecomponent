import { Button, Space } from "antd";
import dayjs from "dayjs";
const columns2 = [
  {
    title: "ID",
    dataIndex: "BOOKING_ID",
    key: "BOOKING_ID",
    width: "100px",
    valueGetter: [(text) => `ID ${text}`],
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
  {
    title: "No.",
    dataIndex: "BOOKING_NO",
    key: "BOOKING_NO",
    width: "200px",
    expand: {
      level:0,
      dataSource: "BOOKING_DTL_INFO",
      columns: [{ source: "DELIVERY_DATE", destination: "DELIVERY_DATE" }],
    },
    customElement: [
      (text, record) => (
        <Space>
          <Button>{text}</Button>
          {`${record.CV_CODE} - ${record.CV_NAME}`}
        </Space>
      ),
    ],
    valueGetter: [(text) => `ID:${text}`],
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
    columnSpan: [2],
  },
  {
    title: "วันที่ขนส่ง",
    dataIndex: "DELIVERY_DATE",
    key: "DELIVERY_DATE",
    width: "200px",
    expand: {
      level:1,
      dataSource: "ITR_BOOKING_DTL",
      columns: [{ source: "DELIVERY_DATE", destination: "DELIVERY_DATE" }],
    },
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
    valueGetter: [
      null,
      (text) => dayjs(text).format("DD/MM/YY"),
      (text) => dayjs(text).format("DD/MM/YYYY"),
    ],
    customElement: [null, (text) => <Button>{text}</Button>],
    columnSpan: [0],
  },
  {
    title: "เลขที่ทริป",
    dataIndex: "TRIP_NO",
    key: "TRIP_NO",
    width: "200px",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
  {
    title: "ทะเบียนรถ(หัว)",
    dataIndex: "LICENSE_PLATE",
    key: "LICENSE_PLATE",
    width: "200px",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
  {
    title: "อัตราค่าขนส่ง",
    dataIndex: "FREIGHT_RATE",
    key: "FREIGHT_RATE",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
  {
    title: "ผู้จองรถ",
    dataIndex: "BOOKING_NAME",
    key: "BOOKING_NAME",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
    customElement: [
      null,
      null,
      (text) => <Button type="dashed">{text}</Button>,
    ],
  },
  {
    title: "เบอร์ติดต่อผู้จองรถ",
    dataIndex: "BOOKING_TELEPHONE",
    key: "BOOKING_TELEPHONE",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
  {
    title: "สถานะจองรถ",
    dataIndex: "STATUS",
    key: "STATUS",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
  {
    title: "ชื่อผู้เปิดทริป",
    dataIndex: "BOOKING_OPEN",
    key: "BOOKING_OPEN",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
  {
    title: "หมายเหตุ",
    dataIndex: "REMARK",
    key: "REMARK",
    columnStyle: [
      { backgroundColor: "#EAF2F8" },
      { backgroundColor: "#F5FAFE" },
    ],
  },
];

export default columns2;
