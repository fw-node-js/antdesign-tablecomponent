import { SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space } from "antd";
import dayjs from "dayjs";
import "./App.css";
import TableComponent from "./components/Table";
import { QueryClient, QueryClientProvider } from "react-query";
import CardTable from "./view/CardTable";

function App() {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <CardTable />
      </div>
    </QueryClientProvider>
  );
}

export default App;
