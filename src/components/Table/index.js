/* eslint-disable consistent-return */
/* eslint-disable prefer-const */
import { DownOutlined, RightOutlined, SearchOutlined } from '@ant-design/icons';
import {
  Table, Space, Button, Input,
} from 'antd';
import { useEffect, useState } from 'react';

function TableFilterExpands(props) {
  const {
    data, columns, rowKey, expand, scroll, loading,
  } = props;

  let [searchParams, setSerachParams] = useState();
  let [dataRows, setDataRows] = useState(
    data.map((item) => ({
      ...item,
      expandKey: 0,
      id: `${item[rowKey]}`,
      key: `${item[rowKey]}`,
    })),
  );

  const [allRows, setAllRows] = useState(() => {
    const set = data.map((item) => {
      const columnValueGetter = columns.filter((rowCol) => {
        if (Object.keys(item).includes(rowCol.dataIndex)
        && rowCol?.valueGetter !== undefined
        && rowCol?.valueGetter[0] !== undefined) {
          return true;
        }
        return false;
      });
      columnValueGetter.forEach((col) => {
        const newGetter = col.valueGetter[1](item[col.dataIndex]);
        item = { ...item, [col.dataIndex]: newGetter };
      });
      let rows = [];
      const defaultRows = {
        ...item,
        expandKey: 0,
        id: `${item[rowKey]}`,
        key: `${item[rowKey]}`,
      };
      const coll = Object.keys(item);
      const collExpand1 = expand.filter(
        (rowExpan, keyExpand) => coll.includes(rowExpan.set) && keyExpand === 0,
      )[0].set;

      if (defaultRows[collExpand1] !== undefined) {
        const expand1 = defaultRows[collExpand1].map((item1, keyExpand1) => {
          const columnValueGetter1 = columns.filter((rowCol) => {
            if (Object.keys(item1).includes(rowCol.dataIndex)
            && rowCol?.valueGetter !== undefined
            && rowCol?.valueGetter[1] !== undefined) {
              return true;
            }
            return false;
          });
          columnValueGetter1.forEach((col) => {
            const newGetter = col.valueGetter[1](item1[col.dataIndex]);
            item1 = { ...item1, [col.dataIndex]: newGetter };
          });
          let rows1 = [];
          const defaultRows1 = {
            ...item1,
            expandKey: 1,
            id: `${item[rowKey]}${keyExpand1}`,
            key: `${item[rowKey]}${keyExpand1}`,
            tree: [defaultRows.id],
          };
          const coll1 = Object.keys(item1);
          const collExpand2 = expand.filter(
            (rowExpan, keyExpand) => coll1.includes(rowExpan.set) && keyExpand === 1,
          )[0].set;

          if (defaultRows1[collExpand2] !== undefined) {
            const expand2 = defaultRows1[collExpand2].map(
              (item2, keyExpand2) => {
                const columnValueGetter2 = columns.filter((rowCol) => {
                  if (Object.keys(item2).includes(rowCol.dataIndex)
                  && rowCol?.valueGetter !== undefined
                  && rowCol?.valueGetter[2] !== undefined) {
                    return true;
                  }
                  return false;
                });
                columnValueGetter2.forEach((col) => {
                  const newGetter = col.valueGetter[2](item2[col.dataIndex]);
                  item2 = { ...item2, [col.dataIndex]: newGetter };
                });
                return {
                  ...item2,
                  expandKey: 2,
                  tree: [defaultRows.id, defaultRows1.id],
                  id: `${item[rowKey]}${keyExpand1}${keyExpand2}`,
                  key: `${item[rowKey]}${keyExpand1}${keyExpand2}`,
                };
              },
            );
            rows1 = rows1.concat(defaultRows1, expand2);
          }
          return { ...rows1 };
        });
        const flatSet1 = expand1.flat();
        const result1 = flatSet1.flatMap((obj) => Object.values(obj));
        rows = rows.concat(defaultRows, result1);
      }
      return { ...rows };
    });
    const flatSet = set.flat();
    const result = flatSet.flatMap((obj) => Object.values(obj));
    return result;
  });

  const handleResetFilter = (clearFilters) => {
    clearFilters();
    setSerachParams(null);
  };

  const getColumnSearchProps = (dataIndex, title, expandKey) => ({
    // eslint-disable-next-line react/no-unstable-nested-components
    filterDropdown: ({
      selectedKeys,
      clearFilters,
      close,
    }) => (
      // eslint-disable-next-line jsx-a11y/no-static-element-interactions
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          placeholder={`Search ${title}`}
          value={selectedKeys[0]}
          onChange={(e) => {
            setSerachParams({
              ...searchParams,
              [dataIndex]: { value: e.target.value, expandKey },
            });
            searchParams = {
              ...searchParams,
              [dataIndex]: { value: e.target.value, expandKey },
            };
          }}
          // onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: 'block',
          }}
        />
        <Space>
          <Button
            type="primary"
            // eslint-disable-next-line no-use-before-define
            onClick={() => onFilter()}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 90,
            }}
          >
            Search
          </Button>
          <Button
            size="small"
            style={{
              width: 90,
            }}
            // eslint-disable-next-line no-use-before-define
            onClick={() => { handleResetFilter(clearFilters); clearFilters(); }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    // eslint-disable-next-line react/no-unstable-nested-components
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? '#1890ff' : undefined,
        }}
      />
    ),
    // onFilter: (value, record) =>
    //   record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    // onFilterDropdownOpenChange: (visible) => {
    //   if (visible) {
    //     setTimeout(() => searchInput.current?.select(), 100);
    //   }
    // },
    // render: (text) =>
    //   searchedColumn === dataIndex ? (
    //     <Highlighter
    //       highlightStyle={{
    //         backgroundColor: '#ffc069',
    //         padding: 0,
    //       }}
    //       searchWords={[searchText]}
    //       autoEscape
    //       textToHighlight={text ? text.toString() : ''}
    //     />
    //   ) : (
    //     text
    //   ),
  });

  const [tableColumns] = useState(
    columns.map((item) => {
      const checkExpand = expand?.findIndex(
        (value) => item.dataIndex === value.inner,
      );
      let column = { ...item };
      if (item.customRender !== undefined) {
        column = {
          ...column,
          render: (text, record, key) => {
            if (
              item.customRender[record.expandKey] !== undefined
              && item.customRender[record.expandKey] !== null
            ) {
              const CellComponent = () => item.customRender[record.expandKey](text, record, key)
                ?.children
                || item.customRender[record.expandKey](text, record, key);
              return {
                props:
                  item.customRender[record.expandKey](text, record, key)
                    ?.props || null,
                children: (
                  <Space>
                    {checkExpand >= 0
                      && record?.expandKey === checkExpand
                      && (record.expand ? <DownOutlined /> : <RightOutlined />)}
                    <CellComponent />
                  </Space>
                ),
              };
            }
            return text;
          },
          onCell: (record, rowIndex) => {
            let response = null;
            if (checkExpand >= 0 && record?.expandKey === checkExpand) {
              // eslint-disable-next-line no-use-before-define
              response = { ...response, onClick: () => onExpand(record, rowIndex) };
            }
            if (item?.columnSpan !== undefined
              && item?.columnSpan[record.expandKey] !== undefined) {
              response = { ...response, colSpan: item.columnSpan[record.expandKey] };
            }
            if (response != null) {
              return response;
            }
          },
        };
      } else if (checkExpand >= 0) {
        column = {
          ...column,
          render: (text, record) => (
            <Space>
              {record?.expandKey === checkExpand
                && (record.expand ? <DownOutlined /> : <RightOutlined />)}
              {record[item.dataIndex]}
            </Space>
          ),
          // eslint-disable-next-line consistent-return
          onCell: (record, rowIndex) => {
            let response = null;
            if (checkExpand >= 0 && record?.expandKey === checkExpand) {
              // eslint-disable-next-line no-use-before-define
              response = { ...response, onClick: () => onExpand(record, rowIndex) };
            }
            if (record?.expandKey === checkExpand
              && item?.columnSpan !== undefined
              && item?.columnSpan[record.expandKey] !== undefined) {
              response = { ...response, colSpan: item.columnSpan[record.expandKey] };
            }
            if (response != null) {
              return response;
            }
          },
        };
      } else {
        column = {
          ...column,
          // eslint-disable-next-line consistent-return
          onCell: (record) => {
            let response = null;
            if (record?.expandKey === checkExpand
              && item?.columnSpan !== undefined
              && item?.columnSpan[record.expandKey] !== undefined) {
              response = { ...response, colSpan: item.columnSpan[record.expandKey] };
            }

            if (response != null) {
              return response;
            }
          },
        };
      }
      if (item.search) {
        const expandKey = item.search(item);
        column = {
          ...column,
          ...getColumnSearchProps(item.dataIndex, item.title, expandKey),
        };
      }
      return column;
    }),
  );

  const onFilter = () => {
    if (searchParams !== undefined) {
      const rowsFilter = allRows.filter((row) => {
        const rowColumn = Object.keys(row);
        const collFilter = Object.entries(searchParams).filter((f) => rowColumn.includes(f[0]));
        const valueFilter = rowColumn.filter((v) => {
          const columnsFilter = collFilter.map((i) => i[0]);
          if (
            columnsFilter.includes(v)
            && row[v].indexOf(searchParams[v].value) >= 0
            && searchParams[v].expandKey === row.expandKey
          ) {
            return true;
          }
          return false;
        });
        return valueFilter.length === Object.keys(searchParams).length;
      });

      let allId = [...new Set(rowsFilter.map((obj) => obj.tree).flat())];
      allId = allId.concat(rowsFilter.map((obj) => obj.id).flat());
      const resultFilter = allRows.filter((row) => allId.includes(row.id))
        .map((item) => ({ ...item, expand: 1 }));
      dataRows = resultFilter;
      setDataRows(resultFilter);
    }
  };

  const onResetFilter = (dataIndex) => {
    if (searchParams[dataIndex]) {
      delete searchParams[dataIndex];
    }
    setSerachParams(searchParams);
    if (Object.keys(searchParams).length < 1) {
      console.log('reset');
      dataRows = data.map((item) => ({ ...item, expandKey: 0, id: item[rowKey] }));
      setDataRows(
        data.map((item) => ({ ...item, expandKey: 0, id: item[rowKey] })),
      );
    }
  };

  const onExpand = (record, key) => {
    const onwerId = record.id;
    const checkTree = dataRows
      .filter((find) => find?.tree?.includes(onwerId))
      .map((item) => item.id);
    let onwerTree = [];
    if (record.tree) {
      onwerTree = record.tree.concat([onwerId]);
    } else {
      onwerTree.push(onwerId);
    }
    let newSet = dataRows;
    const beginSet = dataRows.slice(0, key + 1);
    const endSet = dataRows.slice(key + 1);
    if (record.expand) {
      newSet = newSet.filter((item) => !checkTree.includes(item.id));
      newSet[key] = {
        ...dataRows[key],
        expand: false,
      };
      setDataRows(newSet);
      dataRows = newSet;
    } else {
      const expandKey = expand[dataRows[key]?.expandKey].set;
      const expandColumn = expand[dataRows[key]?.expandKey].column;
      const expandData = record[expandKey];
      const expandSet = expandData.map((row, k) => {
        const getColumn = expandColumn.map((column) => ({
          [column.inner]: row[column.key],
        }));
        let response = row;
        for (const obj of getColumn) {
          const split = Object.entries(obj)[0];
          response = {
            ...response,
            [split[0]]: split[1],
            expandKey: dataRows[key].expandKey + 1,
            for: (dataRows[key].expandKey + 1).toString(),
            tree: onwerTree,
            id: dataRows[key].id + k.toString(),
            key: dataRows[key].id + k.toString(),
          };
        }
        return response;
      });

      const setValueGetter = expandSet.map((item) => {
        const columnValueGetter = columns.filter((rowCol) => {
          if (Object.keys(item).includes(rowCol.dataIndex)
          && rowCol?.valueGetter !== undefined
          && rowCol?.valueGetter[item?.expandKey] !== undefined) {
            return true;
          }
          return false;
        });
        columnValueGetter.forEach((col) => {
          const newGetter = col.valueGetter[item?.expandKey](item[col.dataIndex]);
          item = { ...item, [col.dataIndex]: newGetter };
        });
        return item;
      });
      newSet = beginSet.concat(setValueGetter, endSet);
      newSet[key] = {
        ...dataRows[key],
        expand: true,
      };
      setDataRows(newSet);
      dataRows = newSet;
    }
  };

  useEffect(() => {
    setDataRows(
      data.map((item) => ({ ...item, expandKey: 0, id: item[rowKey] })),
    );

    setAllRows(() => {
      const set = data.map((item) => {
        let rows = [];
        const defaultRows = {
          ...item,
          expandKey: 0,
          id: `${item[rowKey]}`,
          key: `${item[rowKey]}`,
        };
        const coll = Object.keys(item);
        const collExpand1 = expand.filter(
          (rowExpan, keyExpand) => coll.includes(rowExpan.set) && keyExpand === 0,
        )[0].set;

        if (defaultRows[collExpand1] !== undefined) {
          const expand1 = defaultRows[collExpand1].map((item1, keyExpand1) => {
            let rows1 = [];
            const defaultRows1 = {
              ...item1,
              expandKey: 1,
              id: `${item[rowKey]}${keyExpand1}`,
              key: `${item[rowKey]}${keyExpand1}`,
              tree: [defaultRows.id],
            };
            const coll1 = Object.keys(item1);
            const collExpand2 = expand.filter(
              (rowExpan, keyExpand) => coll1.includes(rowExpan.set) && keyExpand === 1,
            )[0].set;

            if (defaultRows1[collExpand2] !== undefined) {
              const expand2 = defaultRows1[collExpand2].map(
                (item2, keyExpand2) => ({
                  ...item2,
                  expandKey: 2,
                  tree: [defaultRows.id, defaultRows1.id],
                  id: `${item[rowKey]}${keyExpand1}${keyExpand2}`,
                  key: `${item[rowKey]}${keyExpand1}${keyExpand2}`,
                }),
              );
              rows1 = rows1.concat(defaultRows1, expand2);
            }
            return { ...rows1 };
          });
          const flatSet1 = expand1.flat();
          const result1 = flatSet1.flatMap((obj) => Object.values(obj));
          rows = rows.concat(defaultRows, result1);
        }
        return { ...rows };
      });
      const flatSet = set.flat();
      const result = flatSet.flatMap((obj) => Object.values(obj));
      return result;
    });
  }, [data]);

  return (
    <Table
      rowKey="id"
      columns={tableColumns}
      dataSource={dataRows}
      pagination={false}
      scroll={scroll}
      loading={loading}
    />
  );
}
export default TableFilterExpands;
