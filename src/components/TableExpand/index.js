import { DownOutlined, RightOutlined, SearchOutlined } from "@ant-design/icons";
import { Button, Input, Space, Table } from "antd";
import { useEffect, useState } from "react";

const TableExpand = ({ dataSource, columns, rowKey, scroll, onRow }) => {
  const [cols, setCols] = useState([]);
  const [rows, setRows] = useState([]);
  const [rowsExpand, setRowsExpand] = useState([]);
  const [searchParams, setSerachParams] = useState();

  const handleFilter = () => {
    if (searchParams !== undefined) {
      const rowsFilter = rowsExpand.filter((row) => {
        const rowColumn = Object.keys(row);
        const collFilter = Object.entries(searchParams).filter((f) =>
          rowColumn.includes(f[0])
        );
        const valueFilter = rowColumn.filter((v) => {
          const columnsFilter = collFilter.map((i) => i[0]);
          if (
            columnsFilter.includes(v) &&
            searchParams[v] != undefined &&
            row[v].indexOf(searchParams[v]?.value) >= 0 &&
            searchParams[v]?.expandLevel === row?.expandLevel
          ) {
            return true;
          }
          return false;
        });
        return valueFilter.length === Object.keys(searchParams).length;
      });
      let allId = [...new Set(rowsFilter.map((obj) => obj.tree).flat())];
      allId = allId.concat(rowsFilter.map((obj) => obj.id).flat());
      const resultFilter = rowsExpand
        .filter((row) => allId.includes(row.id))
        .map((item) => ({ ...item, expanded: true }));
      setRows(resultFilter);
    }
  };

  const handleResetFilter = (dataIndex) => {
    if (searchParams && searchParams[dataIndex]) {
      delete searchParams[dataIndex];
    }
    setSerachParams({ ...searchParams });
    console.log(searchParams);
    if (Object.keys(searchParams).length > 0) {
      handleFilter();
    } else {
      setRows(
        dataSource?.map((item) => {
          item = {
            ...item,
            id: item[rowKey],
            expandLevel: 0,
            expandLevel: 0,
            expanded: false,
          };
          const columnValueGetter = cols.filter((col) => {
            if (
              Object.keys(item).includes(col.dataIndex) &&
              col?.valueGetter !== undefined &&
              col?.valueGetter[item.expandLevel] !== undefined &&
              col?.valueGetter[item.expandLevel] !== null
            ) {
              return true;
            }
            return false;
          });
          columnValueGetter.forEach((col) => {
            item = {
              ...item,
              [col.dataIndex]: col.valueGetter[item.expandLevel](
                item[col.dataIndex]
              ),
            };
          });
          return item;
        })
      );
    }
  };

  const getColumnSearchProps = (dataIndex, title, expandLevel) => ({
    filterDropdown: ({ selectedKeys, clearFilters, close }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          placeholder={`Search ${title}`}
          value={
            searchParams && searchParams[dataIndex] !== undefined
              ? searchParams[dataIndex].value
              : null
          }
          onChange={(e) => {
            setSerachParams({
              ...searchParams,
              [dataIndex]: { value: e.target.value, expandLevel: expandLevel },
            });
          }}
          onPressEnter={() => handleFilter()}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleFilter()}
            icon={<SearchOutlined />}
            size="small"
            style={{
              width: 90,
            }}
          >
            Search
          </Button>
          <Button
            size="small"
            style={{
              width: 90,
            }}
            onClick={() => handleResetFilter(dataIndex)}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
  });

  const handleExpand = (record, rowIndex, column, event) => {
    let tree = [];
    const checkTree = rows
      .filter((item) => item?.tree?.includes(record.id))
      .map((item) => item.id);
    if (record.tree) {
      tree = record.tree.concat([record.id]);
    } else {
      tree.push(record.id);
    }

    let headerRows = rows.slice(0, rowIndex + 1);
    const footerRows = rows.slice(rowIndex + 1);

    if (record.expanded) {
      setRows(
        rows
          .filter((item) => !checkTree.includes(item.id))
          .map((item, key) => {
            if (key === rowIndex) {
              item = { ...item, expanded: false };
            }
            return item;
          })
      );
    } else {
      headerRows[rowIndex].expanded = true;
      const sourceExpand = record[column.expand.dataSource].map(
        (item, index) => {
          item = {
            ...item,
            id: record.id + index,
            expandLevel: record.expandLevel + 1,
            tree: tree,
          };

          column.expand.columns.forEach((columnMap) => {
            if (item[columnMap.source]) {
              item = {
                ...item,
                [columnMap.destination]: item[columnMap.source],
              };
            }
          });
          const columnValueGetter = cols.filter((col) => {
            if (
              Object.keys(item).includes(col.dataIndex) &&
              col?.valueGetter !== undefined &&
              col?.valueGetter[item.expandLevel] !== undefined &&
              col?.valueGetter[item.expandLevel] !== null
            ) {
              return true;
            }
            return false;
          });
          columnValueGetter.forEach((col) => {
            item = {
              ...item,
              [col.dataIndex]: col.valueGetter[item.expandLevel](
                item[col.dataIndex]
              ),
            };
          });
          return item;
        }
      );
      const takeExpand = headerRows.concat(sourceExpand, footerRows);
      setRows(takeExpand);
    }
  };

  useEffect(() => {
    console.log("==========> TableExpand update dataSource -> ", dataSource);
    setRows(
      dataSource?.map((item) => {
        item = {
          ...item,
          id: item[rowKey],
          expandLevel: 0,
          expandLevel: 0,
          expanded: false,
        };
        const columnValueGetter = cols.filter((col) => {
          if (
            Object.keys(item).includes(col.dataIndex) &&
            col?.valueGetter !== undefined &&
            col?.valueGetter[item.expandLevel] !== undefined &&
            col?.valueGetter[item.expandLevel] !== null
          ) {
            return true;
          }
          return false;
        });
        columnValueGetter.forEach((col) => {
          item = {
            ...item,
            [col.dataIndex]: col.valueGetter[item.expandLevel](
              item[col.dataIndex]
            ),
          };
        });
        return item;
      })
    );
    const expandColumn = columns
      .filter((item) => {
        return item.expand !== undefined;
      })
      .map((item) => item.expand);
    console.log("expandColumn --> ", expandColumn);
    setRowsExpand(() => {
      const set = dataSource?.map((item) => {
        const columnValueGetter = columns.filter((rowCol) => {
          if (
            Object.keys(item).includes(rowCol.dataIndex) &&
            rowCol?.valueGetter !== undefined &&
            rowCol?.valueGetter[0] !== undefined
          ) {
            return true;
          }
          return false;
        });
        columnValueGetter.forEach((col) => {
          item = {
            ...item,
            [col.dataIndex]: col.valueGetter[0](item[col.dataIndex]),
          };
        });

        let rows = [];
        const defaultRows = {
          ...item,
          expandLevel: 0,
          id: `${item[rowKey]}`,
          key: `${item[rowKey]}`,
        };
        const coll = Object.keys(item);
        const expandLevel0 = expandColumn.filter((e) => e.level === 0)[0];

        if (defaultRows[expandLevel0.dataSource] !== undefined) {
          const expand1 = defaultRows[expandLevel0.dataSource].map(
            (item1, keyExpand1) => {
              const columnValueGetter1 = columns.filter((rowCol) => {
                if (
                  Object.keys(item1).includes(rowCol.dataIndex) &&
                  rowCol?.valueGetter !== undefined &&
                  rowCol?.valueGetter[1] !== undefined
                ) {
                  return true;
                }
                return false;
              });
              columnValueGetter1.forEach((col) => {
                const newGetter = col.valueGetter[1](item1[col.dataIndex]);
                item1 = { ...item1, [col.dataIndex]: newGetter };
              });
              let rows1 = [];
              const defaultRows1 = {
                ...item1,
                expandLevel: 1,
                id: `${item[rowKey]}${keyExpand1}`,
                key: `${item[rowKey]}${keyExpand1}`,
                tree: [defaultRows.id],
              };
              const coll1 = Object.keys(item1);
              const expandLevel1 = expandColumn.filter((e) => e.level === 1)[0];

              if (defaultRows1[expandLevel1.dataSource] !== undefined) {
                const expand2 = defaultRows1[expandLevel1.dataSource].map(
                  (item2, keyExpand2) => {
                    const columnValueGetter2 = columns.filter((rowCol) => {
                      if (
                        Object.keys(item2).includes(rowCol.dataIndex) &&
                        rowCol?.valueGetter !== undefined &&
                        rowCol?.valueGetter[2] !== undefined
                      ) {
                        return true;
                      }
                      return false;
                    });
                    columnValueGetter2.forEach((col) => {
                      const newGetter = col.valueGetter[2](
                        item2[col.dataIndex]
                      );
                      item2 = { ...item2, [col.dataIndex]: newGetter };
                    });
                    return {
                      ...item2,
                      expandLevel: 2,
                      tree: [defaultRows.id, defaultRows1.id],
                      id: `${item[rowKey]}${keyExpand1}${keyExpand2}`,
                      key: `${item[rowKey]}${keyExpand1}${keyExpand2}`,
                    };
                  }
                );
                rows1 = rows1.concat(defaultRows1, expand2);
              }
              return { ...rows1 };
            }
          );
          const flatSet1 = expand1.flat();
          const result1 = flatSet1.flatMap((obj) => Object.values(obj));
          rows = rows.concat(defaultRows, result1);
        }
        return { ...rows };
      });
      const flatSet = set?.flat();
      const result = flatSet?.flatMap((obj) => Object.values(obj));
      return result;
    });
  }, [dataSource]);
  useEffect(() => {
    console.log("==========> TableExpand update columns -> ", dataSource);
    setCols(
      columns?.map((item) => {
        item = {
          ...item,
          render: (text, record, key) => {
            const CellComponent = () => {
              if (
                item.customElement &&
                item.customElement[record.expandLevel]
              ) {
                return item.customElement[record.expandLevel](
                  text,
                  record,
                  key
                );
              }
              if (text !== undefined && text !== null) {
                return `${text}`;
              }
              return ``;
            };
            return (
              <>
                <Space>
                  {item.expand &&
                    item.expand.level === record.expandLevel &&
                    (record.expanded ? <DownOutlined /> : <RightOutlined />)}
                  <CellComponent />
                </Space>
              </>
            );
          },
        };
        item = {
          ...item,
          onCell: (record, rowIndex) => {
            let onCellComponent = new Object();
            if (item.expand && item.expand.level === record.expandLevel) {
              onCellComponent = {
                ...onCellComponent,
                onClick: (event) => handleExpand(record, rowIndex, item, event),
              };
            }
            if (
              item.columnStyle &&
              item.columnStyle[record.expandLevel] !== undefined
            ) {
              onCellComponent = {
                ...onCellComponent,
                style: item.columnStyle[record.expandLevel],
              };
            }

            if (
              item.columnSpan &&
              item.columnSpan[record.expandLevel] !== undefined
            ) {
              onCellComponent = {
                ...onCellComponent,
                colSpan: item.columnSpan[record.expandLevel],
              };
            }
            return onCellComponent;
          },
        };
        if (item.search) {
          item = {
            ...item,
            ...getColumnSearchProps(
              item.dataIndex,
              item.title,
              item.search.level
            ),
          };
        }
        return item;
      })
    );
  }, [columns, rows, searchParams]);
  return (
    <>
      <Table
        rowKey="id"
        dataSource={rows}
        columns={cols}
        pagination={false}
        scroll={scroll ? scroll : null}
        onRow={onRow ? onRow : null}
      />
      <a onClick={() => console.log(searchParams, rows, rowsExpand)}>
        show rows
      </a>
    </>
  );
};

export default TableExpand;
